package be.hics.sandbox

fun main(args: Array<String>) {
    prettyPrintInt123(Int.MIN_VALUE + 124)
    prettyPrintInt123(-150)
    prettyPrintInt123(-1)
    prettyPrintInt123(0)
    prettyPrintInt123(123)
    prettyPrintInt123(124)
    prettyPrintInt123(100)
    prettyPrintInt123(150)
    prettyPrintInt123(Int.MIN_VALUE + 123)
    prettyPrintInt123(Int.MIN_VALUE + 100)
    prettyPrintInt123(Int.MIN_VALUE)
    prettyPrintInt123(Int.MIN_VALUE - 1)
}

fun prettyPrintInt123(a: Int) {
    val b: Long = Dinglemouse.int123(a)
    val sum: Int = a + b.toInt()
    println("${a} + ${b} = ${sum}")
}

object Dinglemouse {
    fun int123(a: Int): Long {
        return if ((a >= 0) && (a <= 123))
            123L - a
        else if ((a < 0) && (a > Int.MIN_VALUE + 123))
            123L + a * (-1)
        else if ((a > Int.MIN_VALUE) && (a <= Int.MIN_VALUE + 123))
            a - (Int.MIN_VALUE - 123L)
        else
            Int.MAX_VALUE.toLong() + Int.MAX_VALUE.toLong() - (a - 125L)
    }
}
